package com.hoovi.comparizone.features.screen.login.verifyotp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hoovi.comparizone.R;
import com.hoovi.comparizone.features.base.BaseActivity;
import com.hoovi.comparizone.features.screen.login.enterotp.EnterOtpVM;
import com.hoovi.comparizone.features.screen.login.reciever.SMSReceiver;
import com.hoovi.comparizone.features.screen.selectcategory.SelectCategoryActivity;
import com.hoovi.comparizone.utils.GeneralUtil;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import javax.inject.Inject;

public class VerifyOtpActivity extends BaseActivity
    implements VerifyOtpContract.View, SMSReceiver.OTPReceiveListener,
    OnOtpCompletionListener {
  private static final String EXTRA_OTP_VM = "EXTRA_OTP_VM";
  private EnterOtpVM mEnterOtpVM;
  @BindView(R.id.otp_view) OtpView mOtpView;
  @Inject VerifyOtpContract.Presenter presenter;

  public static Intent newIntent(Context context, EnterOtpVM vm) {
    Intent intent = new Intent(context, VerifyOtpActivity.class);
    intent.putExtra(EXTRA_OTP_VM, vm);
    return intent;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_verify_otp);
    ButterKnife.bind(this);
    getInjection().inject(this);
    initComponent();
    presenter.start(this);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    presenter.onDestroy();
  }

  @Override public void showLoading() {
    showProgressDialog();
  }

  @Override public void hideLoading() {
    dismissProgressDialog();
  }

  @Override public void onSetOtp(String otp) {
    mOtpView.setText(otp);
  }

  @Override public void showError(String errorMessage) {
    Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
  }

  @Override public void goToSelectCategoryPage() {
    finish();
    startActivity(SelectCategoryActivity.newIntent(this));
  }

  @Override public void showErrorMessage(String resMessage) {
    Toast.makeText(VerifyOtpActivity.this, resMessage, Toast.LENGTH_SHORT).show();
  }

  @Override public void showMessage(int enterotp_label_sendingotp) {
    Toast.makeText(this, enterotp_label_sendingotp, Toast.LENGTH_SHORT).show();
  }

  @Override public void onOTPReceived(String otp) {
    String getOtp[] = otp.split("!");
    String finalOtp = getOtp[0].replaceAll("[^0-9]", "").trim();
    if (!GeneralUtil.isStringEmpty(finalOtp)) {
      presenter.onRecieveOtp(finalOtp);
    }
  }

  @Override public void onOTPTimeOut() {
    Toast.makeText(this, R.string.verifyotp_error_otptimeout, Toast.LENGTH_SHORT).show();
  }

  @Override public void onOTPReceivedError(String error) {
    presenter.onRecieveOtpError(error);
  }

  @Override public void onOtpCompleted(String s) {
    presenter.verifyOtp(s, mEnterOtpVM);
  }

  @OnClick(R.id.btn_resend_otp) void onClickResendOtpButton() {
    presenter.onClickResendOtpButton(mEnterOtpVM);
  }

  private void initComponent() {
    if (getIntent().getExtras() != null) {
      mEnterOtpVM = getIntent().getExtras().getParcelable(EXTRA_OTP_VM);
    }
    mOtpView.setCursorVisible(true);
    mOtpView.requestFocus();
    mOtpView.setAnimationEnable(true);
    mOtpView.setOtpCompletionListener(this);
  }
}
