package com.hoovi.comparizone.features.screen.selectcategory;

import com.hoovi.comparizone.R;
import com.hoovi.comparizone.utils.ComparizoneConstants;
import java.util.ArrayList;
import java.util.List;

public class SelectCategoryPresenter implements SelectCategoryContract.Presenter {
  private SelectCategoryContract.View mView;

  @Override public void start(SelectCategoryContract.View view) {
    mView = view;
  }

  @Override public void getData() {
    mView.setData(getDummyList());
  }

  @Override public void onClickCategoryItem(SelectCategoryVM vm) {
    mView.goToHomeActivity(vm);
  }

  private List<SelectCategoryVM> getDummyList() {
    List<SelectCategoryVM> mData = new ArrayList<>();
    mData.add(new SelectCategoryVM(ComparizoneConstants.ProductType.GROCERY, "Grocery",
        R.drawable.grocery196));
    mData.add(new SelectCategoryVM(ComparizoneConstants.ProductType.MEDICINE, "Medicine",
        R.drawable.medicine196));
    return mData;
  }
}
