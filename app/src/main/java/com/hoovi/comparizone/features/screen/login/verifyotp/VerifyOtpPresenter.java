package com.hoovi.comparizone.features.screen.login.verifyotp;

import android.util.Log;
import com.hoovi.comparizone.api.ApiService;
import com.hoovi.comparizone.api.post.QueryBuilder;
import com.hoovi.comparizone.api.response.login.LoginResponse;
import com.hoovi.comparizone.features.screen.login.enterotp.EnterOtpVM;
import com.hoovi.comparizone.utils.GeneralUtil;
import com.hoovi.comparizone.utils.preference.PreferenceStorage;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;

public class VerifyOtpPresenter implements VerifyOtpContract.Presenter {
  private final ApiService mApiService;
  private final PreferenceStorage mPreferenceStorage;
  private VerifyOtpContract.View mView;
  private Disposable mDisposable;

  public VerifyOtpPresenter(ApiService apiService,
      PreferenceStorage preferenceStorage) {
    mApiService = apiService;
    mPreferenceStorage = preferenceStorage;
  }

  @Override public void start(VerifyOtpContract.View view) {
    mView = view;
  }

  @Override public void onClickResendOtpButton(EnterOtpVM enterOtpVM) {
  }

  @Override public void onRecieveOtp(String otp) {
    mView.onSetOtp(otp);
  }

  @Override public void onRecieveOtpError(String error) {
    mView.showError(error);
  }

  @Override public void verifyOtp(String otp, EnterOtpVM enterOtpVM) {
    mView.showLoading();
    Log.e("query",
        QueryBuilder.verifyOtp(mPreferenceStorage.getUserId(), enterOtpVM.getMobileNumber(), otp));
    mApiService.verifyOtp(
        QueryBuilder.verifyOtp(mPreferenceStorage.getUserId(), enterOtpVM.getMobileNumber(), otp))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<HashMap<String, LoginResponse>>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(HashMap<String, LoginResponse> data) {
            mView.hideLoading();
            if (data.get("data") != null
                && data.get("data").getVerifyOtp() != null
                && data.get("data").getVerifyOtp().size() > 0) {
              mPreferenceStorage.setUserLoggedIn(true);
              mView.goToSelectCategoryPage();
            } else if (data.get("errors") != null
                && data.get("errors").getErrors() != null
                && data.get("data").getErrors().size() > 0) {
              mPreferenceStorage.setUserLoggedIn(false);
              mView.showErrorMessage(data.get("errros").getErrors().get(0).getMessage());
            }
          }

          @Override public void onError(Throwable e) {
            mView.showErrorMessage("Invalid Otp");
            mView.hideLoading();
          }

          @Override public void onComplete() {

          }
        });
  }

  @Override public void onDestroy() {
    GeneralUtil.safelyDispose(mDisposable);
  }
}
