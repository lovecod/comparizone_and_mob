package com.hoovi.comparizone.utils.preference;

import android.content.Context;
import com.orhanobut.hawk.Hawk;
import java.util.ArrayList;
import java.util.List;

public class ComparizonePreferenceStorage implements PreferenceStorage {

  private static final String PREF_USER_LOGGED_IN = "PREF_USER_LOGGED_IN";
  private static final String PREF_USER_ID = "PREF_USER_ID";

  public ComparizonePreferenceStorage(Context context) {
    Hawk.init(context).build();
  }

  private static final List<String> DELETE_KEY_LIST = new ArrayList<String>() {{
    add(PREF_USER_LOGGED_IN);
  }};

  @Override public void clear() {
    for (String key : DELETE_KEY_LIST) {
      Hawk.delete(key);
    }
  }

  @Override public void setUserLoggedIn(boolean loggedIn) {
    Hawk.put(PREF_USER_LOGGED_IN, loggedIn);
  }

  @Override public boolean isUserLoggedIn() {
    return Hawk.get(PREF_USER_LOGGED_IN, false);
  }

  @Override public void setUserId(String id) {
    Hawk.put(PREF_USER_ID, id);
  }

  @Override public String getUserId() {
    return Hawk.get(PREF_USER_ID, "");
  }
}
