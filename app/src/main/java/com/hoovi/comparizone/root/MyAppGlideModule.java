package com.hoovi.comparizone.root;

import android.content.Context;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule public final class MyAppGlideModule extends AppGlideModule {

  @Override public void applyOptions(Context context, GlideBuilder builder) {
    final int maxDiskCacheSize = 50 * 1024 * 1024; // 50MB
    builder.setDiskCache(new InternalCacheDiskCacheFactory(context, maxDiskCacheSize));
  }
}
