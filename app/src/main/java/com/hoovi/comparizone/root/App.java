package com.hoovi.comparizone.root;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate;
import com.hoovi.comparizone.BuildConfig;
import com.hoovi.comparizone.module.ApplicationModule;
import timber.log.Timber;

public class App extends Application {

  private ApplicationComponent mApplicationComponent;
  private LocalizationApplicationDelegate mLocalizationApplicationDelegate =
      new LocalizationApplicationDelegate(this);

  @Override public void onCreate() {
    super.onCreate();

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }
    mApplicationComponent = DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .build();
  }

  @Override protected void attachBaseContext(Context base) {
    super.attachBaseContext(mLocalizationApplicationDelegate.attachBaseContext(base));
  }

  @Override public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    mLocalizationApplicationDelegate.onConfigurationChanged(this);
  }

  @Override public Context getApplicationContext() {
    return mLocalizationApplicationDelegate.getApplicationContext(super.getApplicationContext());
  }

  public ApplicationComponent getApplicationComponent() {
    return mApplicationComponent;
  }
}
