package com.hoovi.comparizone.api.response.category;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ListCategoriesItem{

	@SerializedName("catDisable")
	private int catDisable;

	@SerializedName("catSortOrder")
	private int catSortOrder;

	@SerializedName("id")
	private String id;

	@SerializedName("categoryName")
	private String categoryName;

	public int getCatDisable(){
		return catDisable;
	}

	public int getCatSortOrder(){
		return catSortOrder;
	}

	public String getId(){
		return id;
	}

	public String getCategoryName(){
		return categoryName;
	}

	@Override
 	public String toString(){
		return 
			"ListCategoriesItem{" + 
			"catDisable = '" + catDisable + '\'' + 
			",catSortOrder = '" + catSortOrder + '\'' + 
			",id = '" + id + '\'' + 
			",categoryName = '" + categoryName + '\'' + 
			"}";
		}
}