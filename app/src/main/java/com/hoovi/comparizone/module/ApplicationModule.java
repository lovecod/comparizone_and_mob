package com.hoovi.comparizone.module;

import android.content.Context;
import com.hoovi.comparizone.root.App;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module public class ApplicationModule {

  private App mApplication;

  public ApplicationModule(App application) {
    mApplication = application;
  }

  @Provides @Singleton public Context provideContext() {
    return mApplication.getApplicationContext();
  }
}
