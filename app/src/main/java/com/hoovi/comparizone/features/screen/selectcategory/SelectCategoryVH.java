package com.hoovi.comparizone.features.screen.selectcategory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hoovi.comparizone.R;

public class SelectCategoryVH extends RecyclerView.ViewHolder {
  private final Callback mCallback;
  private final Context mContext;
  @BindView(R.id.img_category) ImageView imgCategory;

  public SelectCategoryVH(@NonNull View itemView,
      Callback callback) {
    super(itemView);
    mCallback = callback;
    mContext = itemView.getContext();
    ButterKnife.bind(this, itemView);
  }

  public void onBindView(SelectCategoryVM vm) {
    imgCategory.setImageResource(vm.getProductResImage());
  }

  @OnClick(R.id.container_main) void onClickContainerMain() {
    if (getAdapterPosition() != RecyclerView.NO_POSITION) {
      mCallback.onClickItem(getAdapterPosition());
    }
  }

  public interface Callback {
    void onClickItem(int position);
  }
}
