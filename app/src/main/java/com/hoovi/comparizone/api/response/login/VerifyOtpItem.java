package com.hoovi.comparizone.api.response.login;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class VerifyOtpItem{

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("userId")
	private String userId;

	public String getMobile(){
		return mobile;
	}

	public String getUserId(){
		return userId;
	}

	@Override
 	public String toString(){
		return 
			"VerifyOtpItem{" + 
			"mobile = '" + mobile + '\'' + 
			",userId = '" + userId + '\'' + 
			"}";
		}
}