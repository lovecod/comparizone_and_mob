package com.hoovi.comparizone.api.response.login;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ErrorsItem{

	@SerializedName("path")
	private List<String> path;

	@SerializedName("locations")
	private List<LocationsItem> locations;

	@SerializedName("message")
	private String message;

	public List<String> getPath(){
		return path;
	}

	public List<LocationsItem> getLocations(){
		return locations;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"ErrorsItem{" + 
			"path = '" + path + '\'' + 
			",locations = '" + locations + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}