package com.hoovi.comparizone.utils.widget.circularlistview;

public interface CircularListViewListener {
    void onCircularLayoutFinished(CircularListView circularListView,
        int firstVisibleItem,
        int visibleItemCount,
        int totalItemCount);
}
