package com.hoovi.comparizone.features.screen.home.fragment;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hoovi.comparizone.R;
import com.hoovi.comparizone.features.base.BaseFragment;
import com.hoovi.comparizone.root.App;
import com.hoovi.comparizone.utils.widget.circularlistview.CircularListView;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;

public class HomeFragment extends BaseFragment implements HomeContract.View {
  @BindView(R.id.clv_category) CircularListView lvCategory;

  @Inject
  ViewModelProvider.Factory viewModelFactory;
  @Inject HomeContract.Presenter presenter;
  private HomeViewModel mHomeViewModel;
  private HomeCategoryAdapter mAdapter;

  public static HomeFragment newInstance() {
    return new HomeFragment();
  }

  @Nullable @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_home, container, false);
    ButterKnife.bind(this, view);
    initComponent();
    initViewModel();
    presenter.start(this);
    presenter.onGetCategoryData();
    return view;
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    Activity act = null;
    if (context instanceof Activity) {
      act = (Activity) context;
      ((App) act.getApplication()).getApplicationComponent().inject(this);
    }
  }

  private void initComponent() {
    mAdapter = new HomeCategoryAdapter(Objects.requireNonNull(getContext()),
        new HomeCategoryAdapter.Callback() {
          @Override public void onClickItem(HomeCategoryVM vm) {

          }
        });
    DisplayMetrics displaymetrics = new DisplayMetrics();
    Objects.requireNonNull(getActivity())
        .getWindowManager()
        .getDefaultDisplay()
        .getMetrics(displaymetrics);
    lvCategory.setAdapter(mAdapter);
    lvCategory.setRadius(Math.min(3000, displaymetrics.widthPixels));
    lvCategory.scrollFirstItemToCenter();
  }

  private void initViewModel() {
    mHomeViewModel =
        ViewModelProviders.of(Objects.requireNonNull(getActivity()), viewModelFactory)
            .get(HomeViewModel.class);
  }

  @Override public void showLoading() {
    showProgressDialog();
  }

  @Override public void hideLoading() {
    dismissProgressDialog();
  }

  @Override public void loadDataFromViewModel() {
    mHomeViewModel.getCategories()
        .observe(Objects.requireNonNull(getActivity()), new Observer<List<HomeCategoryVM>>() {
          @Override public void onChanged(@Nullable List<HomeCategoryVM> foodVMS) {
            if (foodVMS != null) {
              mAdapter.addAll(foodVMS);
            }
          }
        });
  }
}
