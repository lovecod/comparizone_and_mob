package com.hoovi.comparizone.features.screen.login.enterotp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.hoovi.comparizone.R;
import com.hoovi.comparizone.api.post.EnterOtpPost;
import com.hoovi.comparizone.features.base.BaseActivity;
import com.hoovi.comparizone.features.screen.login.verifyotp.VerifyOtpActivity;
import com.hoovi.comparizone.utils.animUtils.HomeAnimation;
import javax.inject.Inject;

public class EnterOtpActivity extends BaseActivity implements EnterOtpContract.View {

  @BindView(R.id.edtMobileNo)
  EditText etMobileNumber;
  @BindView(R.id.editTextCountryCode)
  EditText etCountryCode;
  @BindView(R.id.container_main)
  LinearLayout containerMain;

  @Inject EnterOtpContract.Presenter presenter;

  public static Intent newIntent(Context context) {
    return new Intent(context, EnterOtpActivity.class);
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_enterotp);
    getInjection().inject(this);
    ButterKnife.bind(this);
    initComponent();
    presenter.start(this);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    presenter.onDestroy();
  }

  @Override public void showLoading() {
    showProgressDialog(R.string.enterotp_label_sendingotp);
  }

  @Override public void hideLoading() {
    dismissProgressDialog();
  }

  @Override public void goToNextPage(EnterOtpVM vm) {
    startActivity(VerifyOtpActivity.newIntent(this, vm));
  }

  @OnClick(R.id.btn_go) void onClickGoButton() {
    if (etMobileNumber.getText().toString().isEmpty()) {
      Toast.makeText(this, R.string.enterotp_error_enternumber, Toast.LENGTH_SHORT).show();
    } else if (etMobileNumber.getText().toString().length() == 10) {
      EnterOtpPost enterOtpPost = new EnterOtpPost();
      enterOtpPost.setMobileNumber(
          etCountryCode.getText().toString() + etMobileNumber.getText().toString());
      containerMain.startAnimation(HomeAnimation.outToRightAnimation());
      presenter.onClickGoButton(enterOtpPost);
    } else {
      Toast.makeText(this, R.string.enterotp_error_entervalidnumber, Toast.LENGTH_SHORT).show();
    }
  }

  private void initComponent() {
    containerMain.startAnimation(HomeAnimation.inFromLeftAnimation());
    etCountryCode.setText(R.string.enterotp_label_indiancode);
  }
}
