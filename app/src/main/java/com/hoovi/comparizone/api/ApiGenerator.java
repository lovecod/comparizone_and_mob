package com.hoovi.comparizone.api;

import com.hoovi.comparizone.BuildConfig;
import com.hoovi.comparizone.api.utils.AuthInterceptor;
import com.hoovi.comparizone.api.utils.AuthKey;
import com.hoovi.comparizone.api.utils.HeadersInterceptor;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class ApiGenerator {
  private static final int CONNECTION_TIMEOUT = 60;
  private Retrofit mRetrofit;

  @Inject public ApiGenerator(AuthKey authKey) {
    mRetrofit = provideRetrofit(BuildConfig.SERVER_URL, provideClient(authKey));
  }

  private OkHttpClient provideClient(AuthKey authKey) {
    OkHttpClient.Builder okBuilder =
        new OkHttpClient.Builder()
            .addInterceptor(new AuthInterceptor(authKey))
            .addInterceptor(new HeadersInterceptor())
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true);

    if (BuildConfig.DEBUG) {
      HttpLoggingInterceptor httpLogging = new HttpLoggingInterceptor();
      httpLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
      okBuilder.addInterceptor(httpLogging);
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor((messages) ->
          Timber.i(messages));
      interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
    }

    return okBuilder.build();
  }

  private Retrofit provideRetrofit(String baseURL, OkHttpClient client) {
    return new Retrofit.Builder().baseUrl(baseURL)
        .client(client)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build();
  }

  public <T> T createApi(Class<T> clazz) {
    return mRetrofit.create(clazz);
  }
}
