package com.hoovi.comparizone.api.utils;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by EKMYonkusumo on 14/12/2017.
 */
public class HeadersInterceptor implements Interceptor {
  @Override public Response intercept(Chain chain) throws IOException {
    Request request = chain.request();
    request = request.newBuilder()
        .addHeader("Content-type", "application/json")
        .addHeader("accept", "application/json")
        .build();

    return chain.proceed(request);
  }
}
