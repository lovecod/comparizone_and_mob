package com.hoovi.comparizone.features.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.hoovi.comparizone.R;
import com.hoovi.comparizone.root.App;
import com.hoovi.comparizone.root.ApplicationComponent;
import com.hoovi.comparizone.utils.ObjectUtils;

public class BaseActivity extends LocalizationActivity {
  private MaterialDialog mProgressDialog;
  private MaterialDialog mErrorDialog;
  private MaterialDialog mExtraNullDialog;
  private boolean mInBackground;
  private boolean mViewDestroyed;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mViewDestroyed = false;
  }

  @Override public void onResume() {
    super.onResume();
    mInBackground = false;
  }

  @Override protected void onPause() {
    super.onPause();
    mInBackground = true;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    mViewDestroyed = true;
    mProgressDialog = null;
    mErrorDialog = null;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  protected boolean isInBackground() {
    return mInBackground;
  }

  protected void showProgressDialog(String message) {
    if (mProgressDialog == null) {
      mProgressDialog = new MaterialDialog.Builder(this).content(message)
          .progress(true, 0)
          .cancelable(false)
          .build();
    } else {
      mProgressDialog.setContent(message);
    }
    if (!isInBackground()) {
      mProgressDialog.show();
    }
  }

  protected void showProgressDialog() {
    showProgressDialog(R.string.general_label_pleasewait);
  }

  protected void showProgressDialog(@StringRes int messageResId) {
    showProgressDialog(getString(messageResId));
  }

  protected void dismissProgressDialog() {
    if (mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing()) {
      mProgressDialog.dismiss();
    }
  }

  protected void showErrorDialog() {
    showErrorDialog(R.string.general_label_error);
  }

  protected void showErrorDialog(String message) {
    if (mErrorDialog == null) {
      mErrorDialog = new MaterialDialog.Builder(this).title(R.string.general_label_error)
          .content(message)
          .positiveText(R.string.general_label_ok)
          .build();
    } else {
      mErrorDialog.setContent(message);
    }
    if (!isInBackground()) {
      mErrorDialog.show();
    }
  }

  protected void showErrorDialog(@StringRes int messageResId) {
    showErrorDialog(getString(messageResId));
  }

  protected void dismissErrorDialog() {
    if (mErrorDialog != null && mErrorDialog.isShowing() && !isFinishing()) {
      mErrorDialog.dismiss();
    }
  }

  protected void setBackButtonEnabled(boolean enabled) {
    final ActionBar actionBar = getSupportActionBar();
    //Assert.assertNotNull(actionBar);
    actionBar.setDisplayHomeAsUpEnabled(enabled);
    actionBar.setHomeButtonEnabled(enabled);
  }

  protected void setTitleVisible(boolean visible) {
    final ActionBar actionBar = getSupportActionBar();
    //Assert.assertNotNull(actionBar);
    actionBar.setDisplayShowTitleEnabled(visible);
  }

  protected void setFragment(Fragment fragment, String tag) {
    setFragment(R.id.container_fragment, fragment, tag);
  }

  protected void setFragment(Fragment fragment, String tag, boolean addToBackStack) {
    setFragment(R.id.container_fragment, fragment, tag, addToBackStack);
  }

  protected void setFragment(int container, Fragment fragment, String tag) {
    setFragment(container, fragment, tag, true);
  }

  protected void setFragment(int container, Fragment fragment, String tag, boolean addToBackStack) {
    if (!ObjectUtils.equals(tag, getLastBackStackTag())) {
      FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      fragmentTransaction.replace(container, fragment, tag);
      if (addToBackStack) fragmentTransaction.addToBackStack(tag);

      fragmentTransaction.commit();
    }
  }

  protected ApplicationComponent getInjection() {
    return ((App) getApplication()).getApplicationComponent();
  }

  protected boolean isViewDestroyed() {
    return mViewDestroyed;
  }

  private String getLastBackStackTag() {
    final FragmentManager manager = getSupportFragmentManager();
    final int count = manager.getBackStackEntryCount();
    return count > 0 ? manager.getBackStackEntryAt(count - 1).getName() : "";
  }

  protected Fragment findFragmentByTag(String tag) {
    return getSupportFragmentManager().findFragmentByTag(tag);
  }

  protected void showExtraNullErrorDialog() {
    if (mExtraNullDialog == null) {
      mExtraNullDialog = new MaterialDialog.Builder(this).title(R.string.general_label_error)
          .content(R.string.general_error_message)
          .positiveText(R.string.general_label_ok)
          .cancelable(false)
          .onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
              onPositiveExtraNullDialog();
            }
          })
          .build();
    }

    mExtraNullDialog.show();
  }

  protected void onPositiveExtraNullDialog() {
    //override this if you want to do additional things
    finish();
  }
}
