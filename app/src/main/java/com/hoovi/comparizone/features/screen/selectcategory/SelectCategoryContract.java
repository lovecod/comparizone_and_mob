package com.hoovi.comparizone.features.screen.selectcategory;

import java.util.List;

public interface SelectCategoryContract {
  interface View {
    void goToHomeActivity(SelectCategoryVM vm);

    void setData(List<SelectCategoryVM> data);
  }

  interface Presenter {
    void start(View view);

    void getData();

    void onClickCategoryItem(SelectCategoryVM vm);
  }
}
