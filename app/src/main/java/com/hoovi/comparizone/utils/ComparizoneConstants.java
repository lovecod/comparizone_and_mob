package com.hoovi.comparizone.utils;

import android.support.annotation.IntDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ComparizoneConstants {
  public static final String EXTRA_PARCEL = "com.hoovi.comparizone";
  public static final String DATE_DEFAULT_FORMAT = "yyyy-mm-dd hh:mm:ss";
  public static final String DATE_HOOVI_FORMAT = "MMM dd,yyyy hh:mm aa";
  public static final String DATE_SLASH_FORMAT = "dd/MM/yyyy";
  public static final String DATE_YEAR_FORMAT = "YYYY-dd-mm";
  public static final String DATE_FULL_MONTH_FORMAT = "hh:mm:a MMM dd,YYYY";
  public static final String DATE_YEAR_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
  public static final String TIME_FORMAT = "hh:mm aa";

  @IntDef({
      ProductType.GROCERY, ProductType.MEDICINE
  }) @Retention(RetentionPolicy.SOURCE) public @interface ProductType {
    int GROCERY = 1;
    int MEDICINE = 2;
  }
}
