package com.hoovi.comparizone.api.response.login;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class LocationsItem{

	@SerializedName("line")
	private int line;

	@SerializedName("column")
	private int column;

	public int getLine(){
		return line;
	}

	public int getColumn(){
		return column;
	}

	@Override
 	public String toString(){
		return 
			"LocationsItem{" + 
			"line = '" + line + '\'' + 
			",column = '" + column + '\'' + 
			"}";
		}
}