package com.hoovi.comparizone.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.widget.EditText;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import io.reactivex.disposables.Disposable;
import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by sathya on 6/14/2018.
 */

public class GeneralUtil {
  private static final String APP_PACKAGE =
      "https://play.google.com/store/apps/details?id=com.hoovi.comparizone&referrer=utm_content%3D";
  public static final int SHOWCASE_RADIUS = 60;
  public static final float SHOWCASE_ALPHA = 0.78f;
  //public static final float SHOWCASE_ALPHA = 0.56f;
  public static final int SHOWCASE_TEXTSIZE = 20;
  public static final int SHOWCASE_DESCIRPTION_TEXTSIZE = 25;
  private static DecimalFormat sDecimalFormat = new DecimalFormat("#.00");
  private static final float COORDINATE_OFFSET = 0.00002f;

  public static final String PRICE_FORMAT = " #.00";

  public static void showFormError(Context context, List<ValidationError> validationErrors) {
    for (ValidationError validationError : validationErrors) {
      if (validationError.getView() instanceof EditText) {
        ((EditText) validationError.getView()).setError(
            validationError.getCollatedErrorMessage(context));
        ((EditText) validationError.getView()).requestFocus();
      }
    }
  }

  public static Date fromISO8601UTC(String dateStr) {
    TimeZone tz = TimeZone.getTimeZone("UTC");
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    df.setTimeZone(tz);
    try {
      return df.parse(dateStr);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void setSwipeRefreshing(final SwipeRefreshLayout swipeRefresh,
      final boolean refreshing) {
    if (swipeRefresh != null) {
      if (refreshing) {
        swipeRefresh.setRefreshing(true);
      } else {
        swipeRefresh.post(new Runnable() {
          public void run() {
            swipeRefresh.setRefreshing(false);
          }
        });
      }
    }
  }

  public static String getCompleteAddressString(Context mContext, double latitude,
      double longitude) {
    String strAdd = "";
    Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
    try {
      List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
      if (addresses != null) {
        Address returnedAddress = addresses.get(0);
        StringBuilder strReturnedAddress = new StringBuilder("");
        for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
          strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
        }
        strAdd = strReturnedAddress.toString();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return strAdd;
  }

  public static <T> List<T> fromJsonList(String json, Class<T> clazz) {
    Object[] array = (Object[]) java.lang.reflect.Array.newInstance(clazz, 0);
    Gson gson = new Gson();
    array = gson.fromJson(json, array.getClass());
    List<T> list = new ArrayList<T>();
    for (int i = 0; i < array.length; i++)
      list.add(clazz.cast(array[i]));
    return list;
  }

  public static <T> T convertFromJson(Class<T> clazz, String json) {
    Gson gson = new Gson();
    return gson.fromJson(json, clazz);
  }

  public static String replaceSlashSocketResponse(String json) {
    String response = json;
    response = response.replace("\\", "");
    response = response.substring(1, response.length() - 1);
    return response;
  }

  public static String getLocality(Context mContext, double latitude,
      double longitude) {
    String locality = "";
    Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
    try {
      List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
      if (addresses != null) {
        Address returnedAddress = addresses.get(0);
        if (returnedAddress.getLocality() != null) {
          return returnedAddress.getLocality();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return locality;
  }

  public static String getArea(String address, String locality) {
    String areaName = "";
    if (address != null) {
      List<String> items = Arrays.asList(address.split("\\s*,\\s*"));
      for (int i = 0; i < items.size(); i++) {
        if (items.get(i).equalsIgnoreCase(locality.trim())) {
          try {
            areaName = items.get(i - 1);
          } catch (ArrayIndexOutOfBoundsException ignored) {
            areaName = "";
          }
        }
      }
      if (TextUtils.isEmpty(areaName)) {
        if (items.size() > 0) {
          areaName = items.get(0);
        }
      }
    }
    return areaName;
  }

  public static String formatPhoneNumber(String mobileNumber) {
    String number = mobileNumber.replaceAll("[^0-9]", "").trim();
    if (number.length() > 10) {
      return number.substring(2);
    } else {
      return number;
    }
  }

  public static boolean isStringEmpty(String str) {
    return str == null || str.trim().equals("");
  }

  public static String formatPrice(double price) {
    return sDecimalFormat.format(price);
  }

  public static String formatPrice(double price, String format) {
    sDecimalFormat.applyPattern(format);
    return formatPrice(price);
  }

  public static long getTimeIntervalInMinutes(Date startDate, Date endDate) {
    if (startDate != null && endDate != null) {
      long mTimeInterval = endDate.getTime() - startDate.getTime();
      final long mSecondsInMilli = 1000;
      final long mMinutesInMilli = mSecondsInMilli * 60;
      final long mHoursInMilli = mMinutesInMilli * 60;
      final long mDaysInMilli = mHoursInMilli * 24;
      return mTimeInterval / mMinutesInMilli;
    }
    return 0;
  }

  public static long getTimeIntervalInMilliSeconds(Date startDate, Date endDate) {
    if (startDate != null && endDate != null) {
      long mTimeInterval = endDate.getTime() - startDate.getTime();
      final long mSecondsInMilli = 1000;
      final long mMinutesInMilli = mSecondsInMilli * 60;
      final long mHoursInMilli = mMinutesInMilli * 60;
      final long mDaysInMilli = mHoursInMilli * 24;
      return mTimeInterval;
    }
    return 0;
  }

  public static long getTimeIntervalInSeconds(Date startDate, Date endDate) {
    if (startDate != null && endDate != null) {
      long mTimeInterval = endDate.getTime() - startDate.getTime();
      final long mSecondsInMilli = 1000;
      return mTimeInterval / mSecondsInMilli;
    }
    return 0;
  }

  public static long tryParse(Long l) {
    if (l == null) return 0;
    return l;
  }

  public static int tryParse(Integer i) {
    if (i == null) return 0;
    return i;
  }

  public static float tryParse(Float f) {
    if (f == null) return 0;
    return f;
  }

  public static double tryParse(Double d) {
    if (d == null) return 0;
    return d;
  }

  public static double tryParse(String price) {
    if (price == null || TextUtils.isEmpty(price)) {
      return 0;
    } else {
      return Double.parseDouble(price);
    }
  }

  public static String formatPayTmAmount(String amt) {
    amt.replaceAll(",", ".");
    int indexOfDot = amt.indexOf(".");
    if (amt.contains(".") && indexOfDot != 0) {
      amt = amt.substring(0, indexOfDot + 2);
    }
    return amt;
  }

  public static String generateString() {
    String uuid = UUID.randomUUID().toString();
    return uuid.replaceAll("-", "");
  }

  public static String concatUserId(HashSet<String> data) {
    StringBuilder sb = new StringBuilder();
    if (data.size() > 0) {
      for (String vm : data) {
        sb.append(vm);
        sb.append(",");
      }
      return sb.substring(0, sb.length() - 1);
    } else {
      return "";
    }
  }

  public boolean isGooglePlayServicesAvailable(Activity activity) {
    GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
    int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
    if (status != ConnectionResult.SUCCESS) {
      if (googleApiAvailability.isUserResolvableError(status)) {
        googleApiAvailability.getErrorDialog(activity, status, 2404).show();
      }
      return false;
    }
    return true;
  }

  public static String replaceCurrencySymbol(String text) {
    return text.replaceAll("\\u20B9", "");
  }

  public static long getRandomDatabaseRandomNumber() {
    int upper = 4;
    int lower = 0;
    return (long) (Math.random() * (upper - lower)) + lower;
  }

  public static int getColorWithAlpha(int color, float ratio) {
    int newColor = 0;
    int alpha = Math.round(Color.alpha(color) * ratio);
    int r = Color.red(color);
    int g = Color.green(color);
    int b = Color.blue(color);
    newColor = Color.argb(alpha, r, g, b);
    return newColor;
  }

  public static boolean validateDetourToSourceOrPickUpAddress(int detour) {
    return detour < 100;
  }

  public static String getReferralString(String referralId) {
    return
        APP_PACKAGE + referralId;
  }

  public static int getRewardsToReachNextLevel(int value) {
    if (value < 50) {
      return 50 - value;
    } else if (value < 100) {
      return 100 - value;
    } else if (value < 150) {
      return 150 - value;
    } else {
      return 200 - value;
    }
  }

  public static int getRewardProgressValue(int value) {
    if (value < 10) {
      return 10;
    } else if (value < 20) {
      return 19;
    } else if (value < 30) {
      return 29;
    } else {
      return value;
    }
  }

  public static long getFileSizeInMB(File file) {
    return file.length() / 1024; // Size in KB
  }

  public static void safelyDispose(Disposable... disposables) {
    for (Disposable subscription : disposables) {
      if (subscription != null && !subscription.isDisposed()) {
        subscription.dispose();
      }
    }
  }

  public static long getTimeIntervalInHours(Date startDate, Date endDate) {
    if (startDate != null && endDate != null) {
      long mTimeInterval = endDate.getTime() - startDate.getTime();
      final long mSecondsInMilli = 1000;
      final long mMinutesInMilli = mSecondsInMilli * 60;
      final long mHoursInMilli = mMinutesInMilli * 60;
      final long mDaysInMilli = mHoursInMilli * 24;
      return mTimeInterval / mHoursInMilli;
    }
    return 0;
  }
}
