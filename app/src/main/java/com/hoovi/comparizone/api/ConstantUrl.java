package com.hoovi.comparizone.api;

class ConstantUrl {
  static final String MAPS =
      "maps/api/place/nearbysearch/json?sensor=true&key=AIzaSyDekl5FSdt5tY4P86rw5XEv9MAJtHrk3Ck";
  static final String ADD_PLACES =
      "maps/api/place/add/json?key=AIzaSyDekl5FSdt5tY4P86rw5XEv9MAJtHrk3Ck";
  static final String PLACES_DETAILS =
      "maps/api/place/details/json?key=AIzaSyDekl5FSdt5tY4P86rw5XEv9MAJtHrk3Ck";

  private ConstantUrl() {
  }
}
