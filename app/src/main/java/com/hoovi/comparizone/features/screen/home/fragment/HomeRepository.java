package com.hoovi.comparizone.features.screen.home.fragment;

import com.hoovi.comparizone.api.ApiService;
import io.reactivex.Single;
import java.util.List;
import javax.inject.Inject;

public class HomeRepository implements HomeContract.Repository {
  private final HomeRemoteDataSource mRemoteDataSource;

  @Inject public HomeRepository(
      ApiService apiService) {
    mRemoteDataSource = new HomeRemoteDataSource(apiService);
  }

  @Override public Single<List<HomeCategoryVM>> listCategories() {
    return mRemoteDataSource.listCategories();
  }
}
