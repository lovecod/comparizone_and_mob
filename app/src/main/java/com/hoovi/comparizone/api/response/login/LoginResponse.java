package com.hoovi.comparizone.api.response.login;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class LoginResponse {

  @SerializedName("verifyOtp")
  private List<VerifyOtpItem> verifyOtp;

  @SerializedName("errors")
  private List<ErrorsItem> errors;

  public List<VerifyOtpItem> getVerifyOtp() {
    return verifyOtp;
  }

  public List<ErrorsItem> getErrors() {
    return errors;
  }
}