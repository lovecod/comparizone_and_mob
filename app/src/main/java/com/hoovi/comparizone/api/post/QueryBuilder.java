package com.hoovi.comparizone.api.post;

public class QueryBuilder {
  public static String loginUser(EnterOtpPost enterOtpPost) {
    return "mutation{\n"
        + "  login(mobile:\"" + enterOtpPost.getMobileNumber() + "\")\n"
        + "  {\n"
        + "    mobile\n"
        + "    id\n"
        + "  }\n"
        + "}";
  }

  public static String listCategories() {
    return "query{\n"
        + "  listCategories{\n"
        + "    id\n"
        + "    categoryName\n"
        + "    catDisable\n"
        + "    catSortOrder\n"
        + "  }\n"
        + "}";
  }

  public static String verifyOtp(String userId,
      String mobileNumber, String otp) {
    return "mutation{\n"
        + "  verifyOtp(mobile:\"" + mobileNumber + "\",\n"
        + "  otp:" + otp + ",\n"
        + "  userId:" + userId + ")\n"
        + "  {\n"
        + "    mobile\n"
        + "    userId\n"
        + "  }\n"
        + "}";
  }
}
