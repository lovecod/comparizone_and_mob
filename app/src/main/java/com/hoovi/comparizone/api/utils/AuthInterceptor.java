package com.hoovi.comparizone.api.utils;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class AuthInterceptor implements Interceptor {

  private final AuthKey mAuthKey;

  public AuthInterceptor(AuthKey authKey) {
    mAuthKey = authKey;
  }

  @Override public Response intercept(Chain chain) throws IOException {
    Request request = chain.request();
    return chain.proceed(request);
  }
}
