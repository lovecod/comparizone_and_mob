package com.hoovi.comparizone.api.utils;

public class AuthKey {

  private String mTokenType;
  private String mAccessToken;
  private String mRefreshToken;

  public String getTokenType() {
    return mTokenType;
  }

  public void setTokenType(String tokenType) {
    mTokenType = tokenType;
  }

  public String getAccessToken() {
    return mAccessToken;
  }

  public void setAccessToken(String accessToken) {
    mAccessToken = accessToken;
  }

  public String getRefreshToken() {
    return mRefreshToken;
  }

  public void setRefreshToken(String refreshToken) {
    mRefreshToken = refreshToken;
  }

  public String getAuthString() {
    return mTokenType + " " + mAccessToken;
  }
}
