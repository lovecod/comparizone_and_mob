package com.hoovi.comparizone.features.screen.login.enterotp;

import android.os.Parcel;
import android.os.Parcelable;

public class EnterOtpVM implements Parcelable {
  private String mMobileNumber;
  private String mUserId;
  private String mAccessToken;

  protected EnterOtpVM(Parcel in) {
    mMobileNumber = in.readString();
    mUserId = in.readString();
    mAccessToken = in.readString();
  }

  public static final Creator<EnterOtpVM> CREATOR = new Creator<EnterOtpVM>() {
    @Override
    public EnterOtpVM createFromParcel(Parcel in) {
      return new EnterOtpVM(in);
    }

    @Override
    public EnterOtpVM[] newArray(int size) {
      return new EnterOtpVM[size];
    }
  };

  public EnterOtpVM() {

  }

  public String getMobileNumber() {
    return mMobileNumber;
  }

  public void setMobileNumber(String mobileNumber) {
    mMobileNumber = mobileNumber;
  }

  public String getUserId() {
    return mUserId;
  }

  public void setUserId(String userId) {
    mUserId = userId;
  }

  public String getAccessToken() {
    return mAccessToken;
  }

  public void setAccessToken(String accessToken) {
    mAccessToken = accessToken;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(mMobileNumber);
    parcel.writeString(mUserId);
    parcel.writeString(mAccessToken);
  }
}
