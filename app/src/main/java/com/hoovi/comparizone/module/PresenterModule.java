package com.hoovi.comparizone.module;

import com.hoovi.comparizone.api.ApiService;
import com.hoovi.comparizone.features.screen.home.fragment.HomeContract;
import com.hoovi.comparizone.features.screen.home.fragment.HomePresenter;
import com.hoovi.comparizone.features.screen.login.enterotp.EnterOtpContract;
import com.hoovi.comparizone.features.screen.login.enterotp.EnterOtpPresenter;
import com.hoovi.comparizone.features.screen.login.verifyotp.VerifyOtpContract;
import com.hoovi.comparizone.features.screen.login.verifyotp.VerifyOtpPresenter;
import com.hoovi.comparizone.features.screen.selectcategory.SelectCategoryContract;
import com.hoovi.comparizone.features.screen.selectcategory.SelectCategoryPresenter;
import com.hoovi.comparizone.utils.preference.PreferenceStorage;
import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule {

  @Provides EnterOtpContract.Presenter provideOtpPresenter(ApiService apiService,
      PreferenceStorage preferenceStorage) {
    return new EnterOtpPresenter(apiService, preferenceStorage);
  }

  @Provides VerifyOtpContract.Presenter prvoideVerifyOtpPresenter(ApiService apiService,
      PreferenceStorage preferenceStorage) {
    return new VerifyOtpPresenter(apiService, preferenceStorage);
  }

  @Provides SelectCategoryContract.Presenter provideSelectCategoryPresenter() {
    return new SelectCategoryPresenter();
  }

  @Provides HomeContract.Presenter provideHomePresenter() {
    return new HomePresenter();
  }
}
