package com.hoovi.comparizone.features.screen.login.enterotp;

import com.hoovi.comparizone.api.post.EnterOtpPost;

public interface EnterOtpContract {
  interface View {
    void showLoading();

    void hideLoading();

    void goToNextPage(EnterOtpVM vm);
  }

  interface Presenter {
    void start(View view);

    void onClickGoButton(EnterOtpPost enterOtpPost);

    void onDestroy();
  }
}
