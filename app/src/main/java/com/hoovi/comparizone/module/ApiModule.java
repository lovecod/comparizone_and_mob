package com.hoovi.comparizone.module;

import com.hoovi.comparizone.api.ApiGenerator;
import com.hoovi.comparizone.api.ApiService;
import com.hoovi.comparizone.api.utils.AuthKey;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module public class ApiModule {

  @Provides public ApiService provideApiService(ApiGenerator apiModule) {
    return apiModule.createApi(ApiService.class);
  }

  @Provides @Singleton AuthKey provideAuthKey() {
    return new AuthKey();
  }

  @Provides ApiGenerator provideApiGenerator(AuthKey authKey) {
    return new ApiGenerator(authKey);
  }
}
