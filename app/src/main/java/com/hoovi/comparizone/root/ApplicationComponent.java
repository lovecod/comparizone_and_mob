package com.hoovi.comparizone.root;

import com.hoovi.comparizone.features.screen.home.HomeActivity;
import com.hoovi.comparizone.features.screen.home.fragment.HomeFragment;
import com.hoovi.comparizone.features.screen.login.enterotp.EnterOtpActivity;
import com.hoovi.comparizone.features.screen.login.verifyotp.VerifyOtpActivity;
import com.hoovi.comparizone.features.screen.selectcategory.SelectCategoryActivity;
import com.hoovi.comparizone.features.screen.splash.SplashActivity;
import com.hoovi.comparizone.module.ApiModule;
import com.hoovi.comparizone.module.ApplicationModule;
import com.hoovi.comparizone.module.PresenterModule;
import com.hoovi.comparizone.module.RepositoryModule;
import com.hoovi.comparizone.module.UtilsModule;
import com.hoovi.comparizone.module.ViewModelModule;
import dagger.Component;
import javax.inject.Singleton;

@Singleton @Component(modules = {
    ApplicationModule.class, PresenterModule.class, ApiModule.class, UtilsModule.class,
    ViewModelModule.class, RepositoryModule.class
}) public interface ApplicationComponent {
  void inject(SplashActivity target);

  void inject(EnterOtpActivity target);

  void inject(HomeFragment target);

  void inject(VerifyOtpActivity target);

  void inject(SelectCategoryActivity target);

  void inject(HomeActivity target);
}
