package com.hoovi.comparizone.features.screen.home.fragment;

import com.hoovi.comparizone.api.ApiService;
import com.hoovi.comparizone.api.post.QueryBuilder;
import com.hoovi.comparizone.api.response.category.ListCategories;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

public class HomeRemoteDataSource implements HomeContract.Repository {
  private final ApiService mApiService;

  @Inject public HomeRemoteDataSource(ApiService apiService) {
    mApiService = apiService;
  }

  @Override public Single<List<HomeCategoryVM>> listCategories() {
    return mApiService.listCategories(QueryBuilder.listCategories()).flatMap(
        new Function<HashMap<String, ListCategories>, SingleSource<? extends List<HomeCategoryVM>>>() {
          @Override public SingleSource<? extends List<HomeCategoryVM>> apply(
              HashMap<String, ListCategories> data) throws Exception {
            return Single.just(HomeCategoryVM.transform(data.get("data").getListCategories()));
          }
        });
  }
}
