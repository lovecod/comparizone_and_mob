package com.hoovi.comparizone.features.screen.home.fragment;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;

public class HomeViewModel extends ViewModel {
  private MutableLiveData<List<HomeCategoryVM>> mData;

  @NonNull
  private final HomeRepository mRepository;

  @Inject public HomeViewModel(
      @NonNull HomeRepository repository) {
    mRepository = repository;
  }

  public LiveData<List<HomeCategoryVM>> getCategories() {
    if (mData == null) {
      mData = new MutableLiveData<List<HomeCategoryVM>>();
      loadCategoryData();
    }
    return mData;
  }

  @SuppressLint("CheckResult") private void loadCategoryData() {
    mRepository.listCategories().subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(foodVMS -> mData.setValue(foodVMS));
  }
}
