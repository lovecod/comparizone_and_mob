package com.hoovi.comparizone.features.screen.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.hoovi.comparizone.R;
import com.hoovi.comparizone.features.base.BaseActivity;
import com.hoovi.comparizone.features.screen.home.fragment.HomeFragment;
import com.hoovi.comparizone.features.screen.login.enterotp.EnterOtpActivity;
import com.hoovi.comparizone.features.screen.selectcategory.SelectCategoryVM;
import com.hoovi.comparizone.utils.ComparizoneConstants;
import com.hoovi.comparizone.utils.preference.PreferenceStorage;
import javax.inject.Inject;

public class HomeActivity extends BaseActivity {
  private static final String EXTRA_CATEGORY_VM =
      ComparizoneConstants.EXTRA_PARCEL + "EXTRA_CATEGORY_VM";
  private static final String HOME_FRAGMENT_TAG = "HOME_FRAGMENT_TAG";
  @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.nav_view) NavigationView navigationView;
  private SelectCategoryVM mSelectCategoryVM;
  private MenuItem mCartItem;
  private MenuItem mSelectedMenu;
  private ActionBarDrawerToggle mDrawerToggle;
  @Inject PreferenceStorage preferenceStorage;

  public static Intent newIntent(Context context) {
    Intent homeIntent = new Intent(context, HomeActivity.class);
    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    return homeIntent;
  }

  public static Intent newIntent(Context context, SelectCategoryVM vm) {
    Intent homeIntent = new Intent(context, HomeActivity.class);
    homeIntent.putExtra(EXTRA_CATEGORY_VM, vm);
    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    return homeIntent;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home_screen);
    ButterKnife.bind(this);
    getInjection().inject(this);
    initComponent();
    showHomeFragment();
  }

  @Override protected void onPostCreate(@Nullable Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    //mDrawerToggle.syncState();
  }

  @Override public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    //mDrawerToggle.onConfigurationChanged(newConfig);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        drawerLayout.openDrawer(GravityCompat.START);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void initComponent() {
    if (getIntent().getExtras() != null) {
      mSelectCategoryVM = getIntent().getExtras().getParcelable(EXTRA_CATEGORY_VM);
      if (mSelectCategoryVM != null) {
        setToolbarTitle(mSelectCategoryVM.getProductName());
      }
    }
    //setSupportActionBar(toolbar);
    //ImageView btnDrawerMenu = toolbar.findViewById(R.id.img_menu);
    //btnDrawerMenu.setImageResource(R.drawable.menu);
    navigationView.setNavigationItemSelectedListener(
        new NavigationView.OnNavigationItemSelectedListener() {
          @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            mSelectedMenu = item;
            drawerLayout.closeDrawers();
            return true;
          }
        });
    //mDrawerToggle =
    //    new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navdrawer_label_open,
    //        R.string.navdrawer_label_close);
    //drawerLayout.addDrawerListener(mDrawerToggle);
    //mDrawerToggle.setDrawerIndicatorEnabled(false);
    //btnDrawerMenu.setOnClickListener(new View.OnClickListener() {
    //  @Override public void onClick(View v) {
    //    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
    //      drawerLayout.closeDrawers();
    //    } else {
    //      drawerLayout.openDrawer(GravityCompat.START);
    //    }
    //  }
    //});

    drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
      @Override public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

      }

      @Override public void onDrawerOpened(@NonNull View drawerView) {

      }

      @Override public void onDrawerClosed(@NonNull View drawerView) {
        navigateTo(mSelectedMenu);
      }

      @Override public void onDrawerStateChanged(int newState) {

      }
    });
  }

  private void navigateTo(MenuItem menuItem) {
    if (menuItem == null) return;
    switch (menuItem.getItemId()) {
      case R.id.nav_home:
        setToolbarTitleFromResource(R.string.nav_home);
        showHomeFragment();
        break;

      case R.id.nav_logout:
        logoutSession();
        break;
    }
    menuItem.setChecked(true);
  }

  private void showHomeFragment() {
    HomeFragment fragment = (HomeFragment) findFragmentByTag(HOME_FRAGMENT_TAG);
    if (fragment == null) {
      fragment = HomeFragment.newInstance();
    }
    setFragment(fragment, HOME_FRAGMENT_TAG);
  }

  private void setToolbarTitle(String title) {
    ((TextView) toolbar.findViewById(R.id.txt_title)).setText(title);
  }

  private void setToolbarTitleFromResource(int title) {
    ((TextView) toolbar.findViewById(R.id.txt_title)).setText(title);
  }

  @Override public void onBackPressed() {
    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
      drawerLayout.closeDrawers();
    } else {
      showExitAppDialog();
    }
  }

  private void showExitAppDialog() {
    new MaterialDialog.Builder(this).content(R.string.general_label_exit)
        .positiveText(R.string.general_label_ok)
        .negativeText(R.string.general_label_cancel)
        .onPositive(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            finish();
          }
        })
        .show();
  }

  private void logoutSession() {
    new MaterialDialog.Builder(this).title(R.string.LOGOUT_WARNING)
        .positiveText(R.string.home_label_logout)
        .negativeText(R.string.general_label_cancel)
        .onPositive((dialog, which) -> {
          preferenceStorage.clear();
          finish();
          startActivity(EnterOtpActivity.newIntent(HomeActivity.this));
        })
        .show();
  }
}
