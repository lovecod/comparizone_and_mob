package com.hoovi.comparizone.features.screen.login.verifyotp;

import com.hoovi.comparizone.features.screen.login.enterotp.EnterOtpVM;

public interface VerifyOtpContract {
  interface View {
    void showLoading();

    void hideLoading();

    void onSetOtp(String otp);

    void showError(String errorMessage);

    void goToSelectCategoryPage();

    void showErrorMessage(String resMessage);

    void showMessage(int enterotp_label_sendingotp);
  }

  interface Presenter {
    void start(View view);

    void onClickResendOtpButton(EnterOtpVM enterOtpVM);

    void onRecieveOtp(String otp);

    void onRecieveOtpError(String error);

    void verifyOtp(String otp, EnterOtpVM enterOtpVM);

    void onDestroy();
  }
}
