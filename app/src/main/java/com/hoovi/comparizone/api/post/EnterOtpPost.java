package com.hoovi.comparizone.api.post;

public class EnterOtpPost {
  private String mobileNumber;

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }
}
