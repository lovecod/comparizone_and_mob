package com.hoovi.comparizone.features.screen.home.fragment;

public class HomePresenter implements HomeContract.Presenter {
  private HomeContract.View mView;

  @Override public void start(HomeContract.View view) {
    mView = view;
  }

  @Override public void onGetCategoryData() {
    mView.loadDataFromViewModel();
  }
}
