package com.hoovi.comparizone.api;

import com.hoovi.comparizone.api.response.UserLogin;
import com.hoovi.comparizone.api.response.category.ListCategories;
import com.hoovi.comparizone.api.response.login.LoginResponse;
import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.HashMap;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
  @POST("/graphql")
  Observable<HashMap<String, UserLogin>> createUser(
      @Query("query") String query);

  @POST("/graphql")
  Observable<HashMap<String, LoginResponse>> verifyOtp(
      @Query("query") String query);

  @POST("/graphql")
  Single<HashMap<String, ListCategories>> listCategories(@Query("query") String query);
}
