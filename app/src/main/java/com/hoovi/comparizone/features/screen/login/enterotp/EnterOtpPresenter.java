package com.hoovi.comparizone.features.screen.login.enterotp;

import com.hoovi.comparizone.api.ApiService;
import com.hoovi.comparizone.api.post.EnterOtpPost;
import com.hoovi.comparizone.api.post.QueryBuilder;
import com.hoovi.comparizone.api.response.UserLogin;
import com.hoovi.comparizone.utils.GeneralUtil;
import com.hoovi.comparizone.utils.preference.PreferenceStorage;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;

public class EnterOtpPresenter implements EnterOtpContract.Presenter {
  private final ApiService mApiService;
  private final PreferenceStorage mPreferenceStorage;
  private EnterOtpContract.View mView;
  private Disposable mDisposable;

  public EnterOtpPresenter(ApiService apiService,
      PreferenceStorage preferenceStorage) {
    mApiService = apiService;
    mPreferenceStorage = preferenceStorage;
  }

  @Override public void start(EnterOtpContract.View view) {
    mView = view;
  }

  @Override public void onClickGoButton(EnterOtpPost enterOtpPost) {
    mView.showLoading();
    mApiService.createUser(QueryBuilder.loginUser(enterOtpPost)).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<HashMap<String, UserLogin>>() {
          @Override public void onSubscribe(Disposable d) {
            mDisposable = d;
          }

          @Override public void onNext(HashMap<String, UserLogin> data) {
            if (data.get("data") != null
                && data.get("data").getLogin() != null
                && data.get("data").getLogin().size() > 0) {
              mPreferenceStorage.setUserId(data.get("data").getLogin().get(0).getId());
              EnterOtpVM enterOtpVM = new EnterOtpVM();
              enterOtpVM.setAccessToken("");
              enterOtpVM.setUserId(data.get("data").getLogin().get(0).getId());
              enterOtpVM.setMobileNumber(enterOtpPost.getMobileNumber());
              mView.goToNextPage(enterOtpVM);
            }
          }

          @Override public void onError(Throwable e) {
            mView.hideLoading();
          }

          @Override public void onComplete() {

          }
        });
  }

  @Override public void onDestroy() {
    GeneralUtil.safelyDispose(mDisposable);
  }
}
