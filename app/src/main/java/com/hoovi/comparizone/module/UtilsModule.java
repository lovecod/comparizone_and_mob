package com.hoovi.comparizone.module;

import android.content.Context;
import com.hoovi.comparizone.utils.preference.ComparizonePreferenceStorage;
import com.hoovi.comparizone.utils.preference.PreferenceStorage;
import dagger.Module;
import dagger.Provides;

@Module public class UtilsModule {
  @Provides public PreferenceStorage providePreferenceStorage(Context context) {
    return new ComparizonePreferenceStorage(context);
  }
}
