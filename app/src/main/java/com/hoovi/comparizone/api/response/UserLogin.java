package com.hoovi.comparizone.api.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UserLogin{

	@SerializedName("login")
	private List<LoginItem> login;

	public List<LoginItem> getLogin(){
		return login;
	}

	@Override
 	public String toString(){
		return 
			"UserLogin{" + 
			"login = '" + login + '\'' + 
			"}";
		}
}