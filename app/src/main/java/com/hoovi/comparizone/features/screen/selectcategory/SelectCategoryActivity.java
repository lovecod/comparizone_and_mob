package com.hoovi.comparizone.features.screen.selectcategory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hoovi.comparizone.R;
import com.hoovi.comparizone.features.base.BaseActivity;
import com.hoovi.comparizone.features.screen.home.HomeActivity;
import com.hoovi.comparizone.utils.RecyclerViewUtils;
import java.util.List;
import javax.inject.Inject;

public class SelectCategoryActivity extends BaseActivity implements SelectCategoryContract.View {

  @BindView(R.id.rv_category) RecyclerView rvCategory;
  private SelectCategoryAdapter mAdapter;

  @Inject SelectCategoryContract.Presenter presenter;

  public static Intent newIntent(Context context) {
    return new Intent(context, SelectCategoryActivity.class);
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_select_category);
    getInjection().inject(this);
    ButterKnife.bind(this);
    initComponent();
    presenter.start(this);
    presenter.getData();
  }

  private void initComponent() {
    rvCategory.setLayoutManager(RecyclerViewUtils.newLinearVerticalLayoutManager(this));
    rvCategory.addItemDecoration(
        RecyclerViewUtils.newVerticalSpacingItemDecoration(this, R.dimen.spacing_30, true, true));
    mAdapter = new SelectCategoryAdapter(new SelectCategoryAdapter.Callback() {
      @Override public void onClickItem(SelectCategoryVM vm) {
        presenter.onClickCategoryItem(vm);
      }
    });
    rvCategory.setAdapter(mAdapter);
  }

  @Override public void goToHomeActivity(SelectCategoryVM vm) {
    finish();
    startActivity(HomeActivity.newIntent(this, vm));
  }

  @Override public void setData(List<SelectCategoryVM> data) {
    mAdapter.setData(data);
  }
}
