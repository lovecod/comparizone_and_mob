package com.hoovi.comparizone.utils.preference;

public interface PreferenceStorage {
  void clear();

  void setUserLoggedIn(boolean loggedIn);

  boolean isUserLoggedIn();

  void setUserId(String id);

  String getUserId();
}
