package com.hoovi.comparizone.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import com.hoovi.comparizone.features.screen.home.fragment.HomeViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import javax.inject.Singleton;

@Module
public abstract class ViewModelModule {
  @Binds
  @IntoMap
  @Singleton
  @ViewModelKey(HomeViewModel.class)
  abstract ViewModel bindViewModel(HomeViewModel homeViewModel);

  @Binds
  @Singleton
  abstract ViewModelProvider.Factory bindViewModelFactory(ComparizoneViewModelFactory factory);
}
