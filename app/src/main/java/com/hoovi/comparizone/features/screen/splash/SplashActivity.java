package com.hoovi.comparizone.features.screen.splash;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.hoovi.comparizone.R;
import com.hoovi.comparizone.features.base.BaseActivity;
import com.hoovi.comparizone.features.screen.login.enterotp.EnterOtpActivity;
import com.hoovi.comparizone.features.screen.selectcategory.SelectCategoryActivity;
import com.hoovi.comparizone.utils.preference.PreferenceStorage;
import javax.inject.Inject;

/**
 * Created by sathya on 6/14/2018.
 */

public class SplashActivity extends BaseActivity {
  @BindView(R.id.img_splash) ImageView imgSplash;
  @Inject PreferenceStorage mPreferenceStorage;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);
    ButterKnife.bind(this);
    getInjection().inject(this);
    new Handler().postDelayed(new Runnable() {
      @Override public void run() {
        finish();
        if (mPreferenceStorage.isUserLoggedIn()) {
          startActivity(SelectCategoryActivity.newIntent(SplashActivity.this));
        } else {
          startActivity(EnterOtpActivity.newIntent(SplashActivity.this));
        }
      }
    }, 2000);
  }
}
