package com.hoovi.comparizone.features.screen.home.fragment;

import io.reactivex.Single;
import java.util.List;

public interface HomeContract {
  interface View {
    void showLoading();

    void hideLoading();

    void loadDataFromViewModel();
  }

  interface Presenter {
    void start(View view);

    void onGetCategoryData();
  }

  interface Repository {
    Single<List<HomeCategoryVM>> listCategories();
  }
}
