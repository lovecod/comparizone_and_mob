package com.hoovi.comparizone.api.response.category;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ListCategories{

	@SerializedName("listCategories")
	private List<ListCategoriesItem> listCategories;

	public List<ListCategoriesItem> getListCategories(){
		return listCategories;
	}

	@Override
 	public String toString(){
		return 
			"ListCategories{" + 
			"listCategories = '" + listCategories + '\'' + 
			"}";
		}
}