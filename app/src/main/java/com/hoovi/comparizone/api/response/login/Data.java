package com.hoovi.comparizone.api.response.login;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("verifyOtp")
	private List<VerifyOtpItem> verifyOtp;

	public List<VerifyOtpItem> getVerifyOtp(){
		return verifyOtp;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"verifyOtp = '" + verifyOtp + '\'' + 
			"}";
		}
}