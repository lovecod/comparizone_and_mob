package com.hoovi.comparizone.features.screen.selectcategory;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hoovi.comparizone.R;
import java.util.ArrayList;
import java.util.List;

public class SelectCategoryAdapter extends RecyclerView.Adapter<SelectCategoryVH> {
  private final Callback mCallback;
  private List<SelectCategoryVM> mData;

  public SelectCategoryAdapter(
      Callback callback) {
    mCallback = callback;
    mData = new ArrayList<>();
  }

  @NonNull @Override
  public SelectCategoryVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View view = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.item_inflate_selectcategory, viewGroup, false);
    return new SelectCategoryVH(view, new SelectCategoryVH.Callback() {
      @Override public void onClickItem(int position) {
        mCallback.onClickItem(mData.get(position));
      }
    });
  }

  @Override public void onBindViewHolder(@NonNull SelectCategoryVH selectCategoryVH, int position) {
    selectCategoryVH.onBindView(mData.get(position));
  }

  @Override public int getItemCount() {
    return mData.size();
  }

  public void setData(
      List<SelectCategoryVM> data) {
    mData.addAll(data);
    notifyDataSetChanged();
  }

  public interface Callback {
    void onClickItem(SelectCategoryVM vm);
  }
}
