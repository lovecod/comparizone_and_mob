package com.hoovi.comparizone.features.screen.selectcategory;

import android.os.Parcel;
import android.os.Parcelable;

public class SelectCategoryVM implements Parcelable {
  private int productType;
  private String productName;
  private Integer productResImage;

  public SelectCategoryVM(int productType, String productName, Integer productResImage) {
    this.productType = productType;
    this.productName = productName;
    this.productResImage = productResImage;
  }

  protected SelectCategoryVM(Parcel in) {
    productType = in.readInt();
    productName = in.readString();
    if (in.readByte() == 0) {
      productResImage = null;
    } else {
      productResImage = in.readInt();
    }
  }

  public static final Creator<SelectCategoryVM> CREATOR = new Creator<SelectCategoryVM>() {
    @Override
    public SelectCategoryVM createFromParcel(Parcel in) {
      return new SelectCategoryVM(in);
    }

    @Override
    public SelectCategoryVM[] newArray(int size) {
      return new SelectCategoryVM[size];
    }
  };

  public Integer getProductResImage() {
    return productResImage;
  }

  public int getProductType() {
    return productType;
  }

  public String getProductName() {
    return productName;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeInt(productType);
    parcel.writeString(productName);
    if (productResImage == null) {
      parcel.writeByte((byte) 0);
    } else {
      parcel.writeByte((byte) 1);
      parcel.writeInt(productResImage);
    }
  }

  @Override public String toString() {
    return "SelectCategoryVM{" +
        "productType=" + productType +
        ", productName='" + productName + '\'' +
        ", productResImage=" + productResImage +
        '}';
  }
}
