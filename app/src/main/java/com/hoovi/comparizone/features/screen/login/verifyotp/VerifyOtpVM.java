package com.hoovi.comparizone.features.screen.login.verifyotp;

public class VerifyOtpVM {
  private String mOtp;

  public String getOtp() {
    return mOtp;
  }

  public void setOtp(String otp) {
    mOtp = otp;
  }
}
