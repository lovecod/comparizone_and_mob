package com.hoovi.comparizone.utils.widget.circularlistview;

public enum CircularListViewContentAlignment {
    None,
    Left,
    Right
}
