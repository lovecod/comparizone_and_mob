package com.hoovi.comparizone.api.response;

import com.google.gson.annotations.SerializedName;

public class LoginItem {

  @SerializedName("mobile")
  private String mobile;

  @SerializedName("id")
  private String id;

  public String getId() {
    return id;
  }

  public String getMobile() {
    return mobile;
  }

  @Override
  public String toString() {
    return
        "LoginItem{" +
            "mobile = '" + mobile + '\'' +
            "}";
  }
}