package com.hoovi.comparizone.features.screen.home.fragment;

import com.hoovi.comparizone.R;
import com.hoovi.comparizone.api.response.category.ListCategoriesItem;
import java.util.ArrayList;
import java.util.List;

public class HomeCategoryVM {
  private String categoryImage;
  private String categoryName;
  private String categoryId;
  private Integer categoryResImage;
  private boolean selected;

  public HomeCategoryVM() {

  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }

  public String getCategoryImage() {
    return categoryImage;
  }

  public void setCategoryImage(String categoryImage) {
    this.categoryImage = categoryImage;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public Integer getCategoryResImage() {
    return categoryResImage;
  }

  public void setCategoryResImage(Integer categoryResImage) {
    this.categoryResImage = categoryResImage;
  }

  public HomeCategoryVM(String categoryName, String categoryId, Integer categoryResImage) {
    this.categoryName = categoryName;
    this.categoryId = categoryId;
    this.categoryResImage = categoryResImage;
  }

  public static List<HomeCategoryVM> transform(List<ListCategoriesItem> data) {
    List<HomeCategoryVM> mData = new ArrayList<>(data.size());
    for (ListCategoriesItem vm : data) {
      HomeCategoryVM homeCategoryVM = new HomeCategoryVM();
      homeCategoryVM.setCategoryId(vm.getId());
      homeCategoryVM.setCategoryName(vm.getCategoryName());
      homeCategoryVM.setCategoryResImage(R.drawable.nuts);
      mData.add(homeCategoryVM);
    }
    return mData;
  }
}