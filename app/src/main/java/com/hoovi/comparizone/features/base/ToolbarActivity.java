package com.hoovi.comparizone.features.base;

import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import com.hoovi.comparizone.R;

public class ToolbarActivity extends BaseActivity {
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override public void setContentView(int layoutResID) {
    CoordinatorLayout containerMaster =
        (CoordinatorLayout) getLayoutInflater().inflate(R.layout.activity_toolbar, null);
    FrameLayout containerContent = containerMaster.findViewById(R.id.container_content);
    getLayoutInflater().inflate(layoutResID, containerContent, true);
    super.setContentView(containerMaster);
    Toolbar mToolbar = findViewById(R.id.toolbar);
    setSupportActionBar(mToolbar);
    setBackButtonEnabled(true);
  }
}
