package com.hoovi.comparizone.features.screen.home.fragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.hoovi.comparizone.R;
import java.util.ArrayList;

public class HomeCategoryAdapter extends ArrayAdapter<HomeCategoryVM> {
  private final Callback mCallback;

  private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
    @Override public void onClick(View view) {
      final int position = (int) view.getTag();
      setSelected(position);
      mCallback.onClickItem(getItem(position));
    }
  };

  public HomeCategoryAdapter(@NonNull Context context,
      Callback callback) {
    super(context, 0, new ArrayList<HomeCategoryVM>());
    mCallback = callback;
  }

  @NonNull @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    HomeCategoryVM vm = getItem(position);
    if (convertView == null) {
      convertView =
          LayoutInflater.from(getContext())
              .inflate(R.layout.item_inflate_category_row, parent, false);
    }
    final TextView txtText = convertView.findViewById(R.id.txt_title);
    final ImageView imgCategory = convertView.findViewById(R.id.img_category);
    final LinearLayout containerItem = convertView.findViewById(R.id.container_item);
    if (vm != null) {
      txtText.setText(vm.getCategoryName());
      imgCategory.setImageResource(vm.getCategoryResImage());
    }
    containerItem.setTag(position);
    containerItem.setOnClickListener(mOnClickListener);
    return convertView;
  }

  private void setSelected(int position) {
    HomeCategoryVM vm;
    for (int i = 0; i < getCount(); i++) {
      vm = getItem(i);
      if (vm != null) {
        vm.setSelected(position == i);
      }
    }
    notifyDataSetChanged();
  }

  public interface Callback {
    void onClickItem(HomeCategoryVM vm);
  }
}
