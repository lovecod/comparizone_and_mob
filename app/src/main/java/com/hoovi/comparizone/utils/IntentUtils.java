package com.hoovi.comparizone.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;

public final class IntentUtils {

  private static final String PACKAGE_SCHEME = "package";
  private static final String CALL_DIALER_TELEPHONE = "tel:";

  public static Intent newSettingsIntent(Context context) {
    Intent intent = new Intent();
    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    Uri uri = Uri.fromParts(PACKAGE_SCHEME, context.getPackageName(), null);
    intent.setData(uri);
    return intent;
  }

  public static Intent newLocationSettingsIntent(Context context) {
    return new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
  }

  public static Intent takePictureIntent(Context context, Uri photoUri) {
    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
      takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
      takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
      return takePictureIntent;
    }
    return null;
  }

  public static Intent openAppInPlayStoreIntent(String appPackageName) {
    try {
      return new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
    } catch (android.content.ActivityNotFoundException e) { // if there is no Google Play available on device
      return new Intent(Intent.ACTION_VIEW,
          Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
    }
  }

  public static Intent openGalleryIntent() {
    Intent pickIntent =
        new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    pickIntent.setType("image/*");
    return pickIntent;
  }

  public static Intent callDialerIntent(String phonenumber) {
    return new Intent(Intent.ACTION_VIEW, Uri.parse(CALL_DIALER_TELEPHONE + phonenumber));
  }

  public static Intent shareAppIntent() {
    Intent sendIntent = new Intent();
    sendIntent.setAction(Intent.ACTION_SEND);
    sendIntent.putExtra(Intent.EXTRA_TEXT,
        "https://play.google.com/store/apps/details?id=com.hoovi.chauffeur");
    sendIntent.setType("text/plain");
    return sendIntent;
  }

  public static Intent openGoogleMapsnavigation(double latitude, double longitude) {
    String uri = "google.navigation:q=" + latitude + "," + longitude +
        "&mode=d";
    Uri gmmIntentUri = Uri.parse(uri);
    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
    mapIntent.setPackage("com.google.android.apps.maps");
    return mapIntent;
  }

  public static Intent openGoogleMapsnavigationWithAddress(String address) {
    String uri = "google.navigation:q=" + address +
        "&mode=d";
    Uri gmmIntentUri = Uri.parse(uri);
    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
    mapIntent.setPackage("com.google.android.apps.maps");
    return mapIntent;
  }
}
