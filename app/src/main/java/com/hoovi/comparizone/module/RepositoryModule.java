package com.hoovi.comparizone.module;

import com.hoovi.comparizone.api.ApiService;
import com.hoovi.comparizone.features.screen.home.fragment.HomeRepository;
import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {
  @Provides HomeRepository provideHomeRepository(ApiService apiService) {
    return new HomeRepository(apiService);
  }
}
